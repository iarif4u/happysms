<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone',15);
            $table->text('msg_text');
            $table->smallInteger('prefix_id')->nullable();
            $table->smallInteger('client_id');
            $table->smallInteger('mobile_id')->nullable();
            $table->smallInteger('msg_status')->default(0);
            $table->dateTime('occur_time')->nullable();
            $table->smallInteger('retry')->nullable();
            $table->lineString('ref_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
