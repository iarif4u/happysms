<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('mobile_id');
            $table->string('socket_id');
            $table->string('brand');
            $table->string('model');
            $table->string('battery_state');
            $table->integer('battery_level');
            $table->string('charging')->default(false);
            $table->string('low_power_mode')->default(false);
            $table->macAddress('mobile_mac')->nullable();
            $table->ipAddress('mobile_ip')->nullable();
            $table->boolean('connected')->default(true);
            $table->boolean('enable')->default(true);
            $table->boolean('status');
            $table->dateTime('touch')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobiles');
    }
}
