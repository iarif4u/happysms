'use strict';
const admin_socket = io(`${server_uri}/admin`, {query: `user_id=${user_id}&email=${user_email}`});

admin_socket.on('connect', (data) => {
    console.log("Admin connection establishment");
    admin_socket.on('mobile-disconnect', (data) => {

        error_notify(`${data.mobile_name} has been disconnected`);
    });
    admin_socket.on('mobile-connect', (data) => {

        success_notify(`${data.mobile_name} has been connected`);
    });

});


let success_notify = (msg) => {
    $('.top-right').notify({
        message: {text: msg},
        closable: false,
        fadeOut: {enabled: true, delay: 3000}
    }).show();
}

let error_notify = (msg) => {
    $('.top-right').notify({
        message: {text: msg},
        closable: false,
        type: 'danger',
        fadeOut: {enabled: true, delay: 3000}
    }).show();
}

let drawCircle = (currentLoad, canvas_id, txt_id) => {
    $(txt_id).text(`${currentLoad}%`);
    var c = document.getElementById(canvas_id);
    var context = c.getContext('2d');
    // Draw Inner Circle
    context.clearRect(0, 0, 500, 500)
    context.fillStyle = "#ccc";
    context.beginPath();
    context.arc(100, 100, 90, Math.PI * 0, Math.PI * 2);
    context.closePath();
    context.fill();

    // Draw the outter line
    // 10px wide line
    context.lineWidth = 10;
    if (currentLoad < 20) {
        context.strokeStyle = '#00ff00';
    } else if (currentLoad < 40) {
        context.strokeStyle = '#337ab7';
    } else if (currentLoad < 60) {
        context.strokeStyle = '#f0ad4e';
    } else {
        context.strokeStyle = '#d9534f';
    }
    context.beginPath();
    context.arc(100, 100, 95, Math.PI * 1.5, (Math.PI * 2 * currentLoad / 100) + Math.PI * 1.5);
    context.stroke();

}

let toHHMMSS = (secs) => {
    var sec_num = parseInt(secs, 10)
    var hours = Math.floor(sec_num / 3600)
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60

    return [hours, minutes, seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v, i) => v !== "00" || i > 0)
        .join(":")
}

let set_cpu_inof = (data) => {

    $(".os-type").text(data.osType);
    $(".time-online").text(toHHMMSS(data.upTime));
    $(".processor-type").text(data.cpuModel);
    $(".processor-core").text(data.numCores);
    $(".processor-speed").text(data.cpuSpeed);
}

let reload_db_table = (className='dataTable') =>{
    $(`.${className}`).DataTable().ajax.reload();
}
