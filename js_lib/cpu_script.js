
const os = require('os');
//admin dashboard connection
let cpu_socket_client = (user_sockets, client, res) => {
    if (user_sockets[client.handshake.query.user_id] == null) {
        console.log(client.handshake.query.email, " Has been connected");
        user_sockets[client.handshake.query.user_id] = 1;
    } else {
        user_sockets[client.handshake.query.user_id] += 1;
    }

    client.on('disconnect', (data) => {
        user_sockets[client.handshake.query.user_id] -= 1;
        if (user_sockets[client.handshake.query.user_id] == 0) {
            console.log(client.handshake.query.email, " Has been disconnected");
        }
    });

    client.on('get_cpu_load', (msg) => {
        performanceData().then((data) => {
            //    const cpu_load = await getCpuLoad();
            client.emit("cpu_load", {cpu_load: data});
            //console.log("Performance ", data);
        })
    });
}

//get cpu performance
let performanceData = () => {
    return new Promise(async (resolve, reject) => {
        const cpus = os.cpus();

        const freeMem = os.freemem();

        const totalMem = os.totalmem();
        const usedMem = totalMem - freeMem;
        const memUse = Math.floor(usedMem / totalMem * 100) / 100;
        const osType = os.type();
        const upTime = os.uptime();
        const cpuModel = cpus[0].model;
        const numCores = cpus.length;
        const cpuSpeed = cpus[0].speed;
        const cpuLoad = await getCpuLoad();

        resolve({
            freeMem,
            totalMem,
            memUse,
            osType,
            upTime,
            cpuModel,
            numCores,
            cpuSpeed,
            cpuLoad
        });
    })
}

let getCpuLoad = () => {
    return new Promise((resolve, reject) => {
        const start = cpuAverage();
        setTimeout(() => {
            const end = cpuAverage();
            const idleDifference = end.idle - start.idle;
            const totalDifference = end.total - start.total;
            const percentageCpu = 100 - Math.floor(100 * idleDifference / totalDifference)

            resolve(percentageCpu);
        }, 100)
    });
}

let cpuAverage = () => {
    const cpus = os.cpus();
    let idleMs = 0;
    let totalMs = 0;
    cpus.forEach((aCore) => {
        for (type in aCore.times) {
            totalMs += aCore.times[type];
        }
        idleMs += aCore.times.idle;
    });

    return {
        idle: idleMs / cpus.length,
        total: totalMs / cpus.length
    }
}

module.exports = {
    cpuAverage,
    getCpuLoad,
    performanceData,
    cpu_socket_client
}


//export import es er keyword egulo support pete webpack+babel laghe
// ekhon module.exports ar require use kor
//thik ache
