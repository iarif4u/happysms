const {URL} = require('./url_script.js');
let new_mobile_connection = (client, io, axios) => {
    io.of('/admin').emit("mobile-connect", client.handshake.query);
    io.of('/mobile').emit("status", client.handshake.query);
    axios.post(`${URL}/api/add-mobile`, {
        mobile_info: client.handshake.query,
        socket: client.id
    })
        .then(function (response) {
            console.log(response.data);
        })
        .catch(function (error) {
            console.log(error);
        });
}

let mobile_dis_connection = (client, io, axios) => {
    console.log("Client disconnected");
    axios.post(`${URL}/api/disconnect-mobile`, {
        socket: client.id
    })
        .then(function (response) {
            //console.log(response);
            io.of('/admin').emit("mobile-disconnect", client.handshake.query);
            io.of('/mobile').emit("status", client.handshake.query);
        })
        .catch(function (error) {
            log.info(error);
        });
}

module.exports = {
    new_mobile_connection,
    mobile_dis_connection
}
