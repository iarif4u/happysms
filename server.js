//make  server by http service
var app = require('express')();
var server = require('http').Server(app);
var {
    cpu_socket_client
} = require('./js_lib/cpu_script.js');

var {new_mobile_connection, mobile_dis_connection} = require('./js_lib/mobile_script.js');
//config socket server
const io = require('socket.io')(server);
const os = require('os');
const ioredis = require('ioredis');
const Queue = require('bee-queue');
const axios = require('axios').default;
const queue = new Queue('sms');
const statusQueue = new Queue('status');
const {URL} = require('./js_lib/url_script.js');
const SimpleNodeLogger = require('simple-node-logger'),
    opts = {
        logFilePath: 'log/log_info_file.log',
        timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
    },
    log = SimpleNodeLogger.createSimpleLogger(opts);

let redis = new ioredis();
let user_sockets = [];
let mobiles = [];
let mobile_sockets = [];
let running_socket = '';
let sequence = 0;

//make connection to client
io.of('/').on('connection', (client, res) => {
    if (hasOwnProperty(client.handshake.query, 'mobile')) {
        new_mobile_connection(client, io, axios);
        //  client.join(nick.room);
        console.log("Mobile id:", client.handshake.query.mobile_id);
        client.join(client.handshake.query.mobile_id)
        var mobileInfo = {};
        mobileInfo.mobileId = client.handshake.query.mobile_id;
        mobileInfo.socketId = client.id;
        mobiles.push(mobileInfo);
        mobile_sockets.push(client.handshake.query.mobile_id);
    }

    client.on('sms_response', (response) => {
        console.log("Emmit Response", response);
        const newjob = queue.createJob({data: response, status: true})
        newjob.save();
        newjob.on('succeeded', (result) => {
            log.info("Result to: ", result);
        });
    })

    client.on('sms_error', (response) => {
        log.info(response);
    })

    // //get message from client
    // client.on('message', (msg) => {
    //     console.log(msg);
    // });

    client.on('disconnect', (data) => {
        if (hasOwnProperty(client.handshake.query, 'mobile')) {
            client.leave(client.handshake.query.mobile_id);
            for (var i = 0, len = mobiles.length; i < len; ++i) {
                var c = mobiles[i];
                if (c.socketId == client.id) {
                    mobiles.splice(i, 1);
                    break;
                }
            }
            var index = mobile_sockets.indexOf(client.handshake.query.mobile_id);
            if (index > -1) {
                mobile_sockets.splice(index);
                mobile_dis_connection(client, io, axios);
            }
        }
        /*console.log("Total mobile", mobile_sockets.length);
        console.log("Disconnected", client.id);*/
    });

});

redis.psubscribe('*', function (err, count) {
    console.log('Subscribed');
});
redis.on('pmessage', function (subscribed, channel, output) {
    output = JSON.parse(output);
    let event = output.event;
    let data = output.data;
    if (event == 'App\\Events\\SMSTerminate') {
        queue_data_msg(data);
    } else {
        //admin connection socket
        io.of('/admin').emit(event, {data: data});
    }
});

function queue_data_msg(data, status = false) {
    const job = queue.createJob({data: data, status: status})
    job.save();
    job.on('succeeded', (result) => {
        if (result.status) {
            if (result.send) {
                const newjob = queue.createJob({data: result, status: true})
                newjob.save();
                newjob.on('succeeded', (result) => {
                    if (result.status) {
                        log.info("Result to: ", result);
                    } else {
                        queue_data_msg(data);
                        log.info("Waiting for mobile connection :", data.actionId);
                    }
                });
            }
            log.info("Result to: ", result.send);
        } else {
            queue_data_msg(data);
            log.info("Waiting for mobile connection :", data.actionId);
        }
    });

}

let update_status = (post_data) => {
    console.log("Post Data", post_data.data);
    axios.post(`${URL}/api/send-mobile`,
        post_data.data
    )
        .then(function (response) {
            console.log("SMS Terminate Response :", response.data);
        })
        .catch(function (error) {
            log.info("Catch on sms terminate ", error);
        });
}

// Process jobs from as many servers or processes as you like
queue.process(function (job, done) {
    //console.log("Process:", job.data);
    if (job.data.status) {
        update_status(job.data);
        return done(null, {
            status: true,
            send: false
        });
    } else {
        log.info("Processing message: ", job.data.data.actionId);
        let current_socket = false;
        if (mobile_sockets.length > 0) {
            var room_id = mobile_sockets[sequence % mobile_sockets.length];
            sequence++;
            if (io.sockets.adapter.rooms[room_id].length > 0) {
                try {
                    let runningSockets = Object.keys(io.sockets.adapter.rooms[room_id].sockets);
                    let fail = true;
                    runningSockets.forEach((socket, myIndex) => {
                        if (io.sockets.connected[socket] != undefined) {
                            io.sockets.connected[socket].emit('sms', job.data, function (response) {
                                if (response) {
                                    done(null, {
                                        status: true, socket_id: room_id,
                                        msg_data: response, send: true
                                    });
                                }else{
                                    done(null, {
                                        status: false, socket_id: room_id,
                                        msg_data: response, send: true
                                    });
                                }
                            });
                        }else{
                            console.log("Waiting for new connection....");
                        }
                    });
                    // io.in(room_id).emit('sms', job.data);

                    /*  io.sockets.connected[current_socket].emit('sms', job.data, function (response) {
                          log.info("Emmit Response", response.actionId);

                      });*/
                } catch (e) {
                    log.info("Error: ", e);
                }
            } else {
                log.info("Socket disconnected");
                return done(null, {status: false});
            }
        } else {
            log.info("Socket not found");
            return done(null, {status: false});
        }
    }

});


function hasOwnProperty(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
        (!(prop in proto) || proto[prop] !== obj[prop]);
}


//send cpu info to client
io.of('/cpu').on('connection', (client, res) => {
    cpu_socket_client(user_sockets, client, res);
});

io.of('/admin').on('connection', (client, res) => {
    console.log("Admin Connection");
});

io.of('/mobile').on('connection', (client, res) => {
    console.log("Mobile Connection for HTML");
});

//when unknown event occur
let unknown_event = (data) => {
    console.log("unknown event", data);
}

//event when user login
let user_login = (data) => {
    console.log("User Loged in", data);
}


let sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

server.listen(3000);
//pm2 start artisan --name laravel-worker --interpreter php -- queue:work --daemonreac  AMTT@007!!
