<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "messages";
    protected $fillable = ['phone', 'msg_text', 'prefix_id', 'client_id', 'mobile_id', 'msg_status', 'occur_time','retry'];

    public function mobile(){
        return $this->hasOne(Mobile::class,'id','mobile_id');
    }
}
