<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    protected $table = "mobiles";

    protected $fillable = ["name","mobile_id","socket_id","brand","model","battery_state","battery_level","charging","low_power_mode","mobile_mac","mobile_ip","connected","enable","status","touch"];

}
