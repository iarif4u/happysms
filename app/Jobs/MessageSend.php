<?php

namespace App\Jobs;

use App\Events\SMSTerminate;
use App\Events\UserLogin;
use App\Model\Message;
use App\Model\Mobile;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class MessageSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //  event(new \App\Events\MessageSend($this->message));
        //$user = User::where('id', '>', 0)->first();
        Redis::publish('test-channel', json_encode($this->message));
        $mobile = Mobile::where(["connected" => true])->orderBy('touch')->first();
        if ($this->message->retry == null) {
            $this->message->retry = 0;
            $this->message->save();
        }
        if ($mobile) {
            $mobile->touch = Carbon::now();
            Message::where(['id' => $this->message->id])->update([
                'msg_status' => 1,
                'mobile_id' => $mobile->id,
                'occur_time' => Carbon::now()
            ]);
            $mobile->save();
            event(new SMSTerminate($this->message, $mobile));
        } else {
            $this->message->increment('retry');
            $this->message->save();
            Message::where(['id' => $this->message->id])->update(['occur_time' => Carbon::now()]);
            $job = (new MessageSend($this->message))->delay(Carbon::now()->addMinute());
            dispatch($job);
        }


    }
}
