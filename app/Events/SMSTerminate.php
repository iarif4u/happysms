<?php

namespace App\Events;

use App\Model\Message;
use App\Model\Mobile;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SMSTerminate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message;
    public $mobile;

    /**
     * Create a new event instance.
     *
     * @param Message $message
     */
    public function __construct(Message $message,Mobile $mobile)
    {
        $this->message = $message;
        $this->mobile = $mobile;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

        return new Channel('test-channel');

    }

    /**
     * Get the data to broadcast.
     *
     * @author Author
     *
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'actionId' => $this->message->id,
            'text' => $this->message->msg_text,
            'number' => $this->message->phone,
            'mobile' => $this->mobile->id,
            'socket_id' => $this->mobile->socket_id,
            'namespace'=>'admin',
            'event'=>'SMSTerminate'
        ];
    }
}
