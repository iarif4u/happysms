<?php

namespace App\Listeners;

use App\Events\NewSMSEvent;
use App\Jobs\MessageSend;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NewSMSEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSMSEvent  $event
     * @return void
     */
    public function handle(NewSMSEvent $event)
    {
        $job = (new MessageSend($event->message))->delay(Carbon::now()->addSeconds(1));
        dispatch($job);

    }
}
