<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    /**
     * Show the application dashboard.
     *
     * @param Illuminate\Http\Request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|phone:BD',
        ], [
            'name.required' => 'Name is required',
            'address.required' => 'Address is required',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
        ]);
        if ($validator->fails()) {
            //return error true, with validation error if has
            return redirect()->back()->withErrors($validator->errors()->all());
        } else {
            try {
                $update_req = $request->all();
                unset($update_req['_token']);
                User::where(['id' => auth()->id()])->update($update_req);
                return redirect()->back()->with('message', "Profile update successfully done");
            } catch (\Exception $exception) {
                return redirect()->back()->withErrors($exception->getMessage());
            }
        }
    }

    public function make_new_api_key()
    {
        $new_key = str_ireplace('-', '', Str::uuid() . time());
        try {
            User::where(['id' => auth()->id()])->update(['api_key' => $new_key]);
            if (\request()->ajax()){
                return response()->json(['error'=>false,"message"=> "New api key generate successfully done",'api_key'=>$new_key]);
            }
            return redirect()->back()->with('message', "New api key generate successfully done");
        } catch (\Exception $exception) {
            if (\request()->ajax()){
                return response()->json(['error'=>true,'message'=>$exception->getMessage()]);
            }
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }
}
