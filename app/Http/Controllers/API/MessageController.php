<?php

namespace App\Http\Controllers\API;


use App\Events\NewSMSEvent;
use App\Jobs\MessageSend;
use App\Model\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    //get message from api request
    public function get_message(Request $request)
    {

        if ($request->accepts('application/json')) {


            $validator = Validator::make($request->all(), [
                'number' => 'required|phone:BD',
                'api_key' => 'required|exists:users,api_key',
                'message' => 'required',
            ], [
                'api_key.required' => 'API key is required',
                'api_key.exists' => 'API key is invalid',
                'number.required' => 'Phone number is required',
                'number.phone' => 'Phone number is invalid',
                'message.required' => 'Message is required',
            ]);
            if ($validator->fails()) {
                //return error true, with validation error if has
                return response()->json(['error' => true, 'error_txt' => $validator->errors()->all()], 400);
            } else {
                $number = $request->input('number');
                $msg_txt = $request->input('message');
                try {
                    $client = User::where(['api_key' => $request->input('api_key')])->first();
                    $message = Message::create([
                        'phone' => $number,
                        'msg_text' => $msg_txt,
                        'client_id' => $client->id,
                        'msg_status' => 0,
                        'retry' => 0
                    ]);
                    event(new NewSMSEvent($message));
                    return response()->json(['error' => false, "txt" => "message accepted", 'sms_id' => $message->id], 200);
                } catch (\Exception $exception) {
                    return response()->json(['error' => true, 'error_txt' => $exception->getMessage()], 406);
                    //return response()->json(['error' => true, 'error_txt' => "Internal error occur"], 406);
                }
            }


        }
    }
}
