<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Model\Message;
use App\Model\Mobile;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_new_mobile(Request $request)
    {

        $mobile = $request->input('mobile_info');
        $socket_id = $request->input('socket');
        $brand = $mobile['brand'];
        $model = $mobile['model'];
        $name = $mobile['mobile_name'];
        $mac_addr = $mobile['macAddr'];
        $is_charging = $mobile['charging'];
        $battery = $mobile['battery_level'];
        $battery_state = $mobile['battery_state'];
        $mobile_id = $mobile['mobile_id'];
        $low_power_mode = $mobile['low_power_mode'];
        $ip_addr = $mobile['ip_addr'];
        try {
            if (Mobile::where(["mobile_id" => $mobile_id])->count() == 0) {
                Mobile::create([
                    "name" => $name,
                    "mobile_id" => $mobile_id,
                    "socket_id" => $socket_id,
                    "brand" => $brand,
                    "model" => $model,
                    "battery_state" => $battery_state,
                    "battery_level" => $battery,
                    "charging" => $is_charging,
                    "low_power_mode" => $low_power_mode,
                    "mobile_mac" => $mac_addr,
                    "mobile_ip" => $ip_addr,
                    "connected" => true,
                    "enable" => true,
                    "status" => true,
                    "touch" => Carbon::now()
                ]);
                return response()->json(['error' => false, 'txt' => "New mobile added successfully done"]);
            } else {
                Mobile::where(["mobile_id" => $mobile_id])->update([
                    "name" => $name,
                    "socket_id" => $socket_id,
                    "battery_state" => $battery_state,
                    "battery_level" => $battery,
                    "charging" => $is_charging,
                    "low_power_mode" => $low_power_mode,
                    "mobile_ip" => $ip_addr,
                    "connected" => true
                ]);
                return response()->json(['error' => false, 'txt' => "Mobile update with new info", "socket" => $socket_id]);
            }

        } catch (\Exception $exception) {
            return response()->json(['error' => true, 'error_txt' => $exception->getMessage()], 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function disconnect_mobile(Request $request)
    {
        $socket_id = $request->input('socket');
        try {
            Mobile::where(["socket_id" => $socket_id])->update([
                "connected" => false,
                "enable" => false,
                "status" => false
            ]);
            return response()->json(['error' => false, 'txt' => "a mobile has been disconnected", "socket_id" => $socket_id]);
        } catch (\Exception $exception) {
            return response()->json(['error' => true, 'error_txt' => $exception->getMessage()], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function send_sms_mobile(Request $request)
    {

        $message_data = $request->input('msg_data');
        $msg_id = $message_data['actionId'];
        $status = $message_data['status'];
        $mobile_id = $request->input(' socket_id');
        try {
            if ($mobile_id) {
                $mobile = Mobile::where(['mobile_id' => $mobile_id])->first();
                if ($mobile) {
                    Message::where(['id' => $msg_id])->update(['mobile_id' => $mobile->id, 'msg_status' => $status, 'occur_time' => Carbon::now()]);
                }
            } else {
                Message::where(['id' => $msg_id])->update(['msg_status' => 5, 'occur_time' => Carbon::now()]);
            }
        } catch (\Exception $exception) {
            Message::where(['id' => $msg_id])->where('msg_status', '<', 3)->update(['msg_status' => $status, 'occur_time' => Carbon::now()]);
        }
        Message::where(['id' => $msg_id])->update(['msg_status' => $status, 'occur_time' => Carbon::now()]);
        return response()->json(['error' => false, "response" => "Message status update", 'message_data' => ['id' => $msg_id, 'status' => $status]]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function show(Mobile $mobile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function edit(Mobile $mobile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mobile $mobile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mobile $mobile)
    {
        //
    }
}
