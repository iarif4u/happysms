<?php

namespace App\Http\Controllers\Admin\SMS;

use App\DataTables\MessagesDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SMSController extends Controller
{
    //get today sms update
    public function sms(MessagesDataTable $dataTable){

        return $dataTable->render('admin.SMS.sms');
    }
}
