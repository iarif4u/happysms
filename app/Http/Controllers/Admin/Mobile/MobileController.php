<?php

namespace App\Http\Controllers\Admin\Mobile;

use App\DataTables\MobilesDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MobileController extends Controller
{
    //get mobile list
    public function mobile(MobilesDataTable $dataTable){
        return $dataTable->render('admin.mobile.mobiles');
    }
}
