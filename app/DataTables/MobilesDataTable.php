<?php

namespace App\DataTables;

use App\Model\Mobile;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MobilesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('battery_state',function ($mobile){
                return ucfirst($mobile->battery_state);
            })
            ->editColumn('connected', function ($mobile) {
                return $mobile->connected ? html()->span()->class('text-success')->text('Connected') : html()->span()->class('text-danger')->text('Not Connected');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Mobile $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Mobile $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('mobiles-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('brand'),
            Column::make('model'),
            Column::make('battery_level')->name('Battery')->title('Battery'),
            Column::make('battery_state'),
            Column::make('mobile_ip')->title('IP'),
            Column::make('connected'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Mobiles_' . date('YmdHis');
    }

    protected function getBuilderParameters()
    {
        return [
            'dom' => '<"top"l>frt<"bottom">Bpi<"clear">',
            'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
            'initComplete' => "function () {
                            this.api().columns([0,1,2,3]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).addClass('form-control wd-full input-sm');
                                $(input).appendTo($(column.footer()).empty())                                
                                .on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
        ];
    }

}
