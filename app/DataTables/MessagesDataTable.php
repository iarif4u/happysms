<?php

namespace App\DataTables;

use App\Model\Message;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MessagesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('msg_status', function ($message) {
                switch ($message->msg_status) {
                    case 1:
                        return 'Processing';
                        break;
                    case 2:
                        return 'Pending';
                        break;
                    case 3:
                        return 'Success';
                        break;
                    case 4:
                        return 'Failed';
                        break;
                    default:
                        return 'Queued';
                        break;
                }
            })
            ->setRowClass(function ($message) {

                switch ($message->msg_status) {
                    case 1:
                        return 'active text-bold msg_' . $message->id;
                        break;
                    case 2:
                        return 'msg_' . $message->id;
                        break;
                    case 3:
                        return 'text-success';
                        break;
                    case 4:
                        return 'text-danger';
                        break;
                    default:
                        return 'text-muted msg_' . $message->id;;
                        break;
                }
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Model\Message $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Message $model)
    {
        return $model->newQuery()->orderByDesc('id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('messages-datatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->info(false)
            ->dom('Bfrtip')
            ->pageLength(25)
            ->processing(false)
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('ID'),
            Column::make('phone')->title('Phone'),
            Column::make('msg_text')->title('Message'),
            Column::make('created_at')->title('Date Time'),
            Column::make('retry')->title('Retry'),
            Column::make('msg_status')->title('Status'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Messages_' . date('YmdHis');
    }


    protected function getBuilderParameters()
    {
        return [
            'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
            'initComplete' => "function () {
                            this.api().columns([0,1,2,3]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).addClass('form-control wd-full input-sm');
                                $(input).appendTo($(column.footer()).empty())                                
                                .on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
        ];
    }
}
