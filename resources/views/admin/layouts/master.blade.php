<?php
/**
 * User: Md. Arif
 * Date: 6/1/2018
 * Time: 4:58 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo.jpg')}}"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('assets/css/ionicons.min.css')}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{asset('assets/css/_all-skins.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/css/AdminLTE.min.css')}}">

    <!-- Custom style -->

    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">

    @yield('style')
    <title>@yield('title')</title>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

{{--include the top menu bar--}}
@include('admin.include.top_sidebar')
{{--include the left side bar--}}
@include('admin.include.left_sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div id="status"></div>
            <h1>
                @yield('header_left')
            </h1>
            <ol class="breadcrumb">
                @yield('header_right')
            </ol>
        </section>

        <div class='notifications top-right'></div>
        <!-- Main content -->
        <section class="content container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <span class="each-error">{{ $error }} </span><br/>
                    @endforeach
                </div>

            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        <!-- Small boxes (Stat box) -->
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    {{--include the footer--}}
    @include('admin.include.footer')
</div>
<!-- ./wrapper -->
@yield('modal')
{{--link the js plugin--}}
@include('admin.include.javascript_bar')
<script>
    // requestAnimationFrame Shim
    (function () {
        var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        window.requestAnimationFrame = requestAnimationFrame;
    })();


    //get server with port
    const server_uri = '{{request()->getHost()}}:3000';
    //config socket by server
    const user_id = '{{auth()->user()->id}}'
    const user_email = '{{auth()->user()->email}}'
    const socket = io(server_uri, {query: `user_id=${user_id}&email=${user_email}`});

</script>
@yield('script')
<script src="{{asset('assets/js/custom.js')}}"></script>
</body>
</html>
