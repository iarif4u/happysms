
<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
   {{-- <div class="pull-right hidden-xs">
        Officer CRM DC Project
    </div>--}}
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}} <a target="_blank" href="javascript:void(0);">Different Coder</a>.</strong> All rights
    reserved.
</footer>

<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
<div class="control-sidebar-bg">
</div>
