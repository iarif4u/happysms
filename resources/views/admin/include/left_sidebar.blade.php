<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--<li class="header">HEADER</li>-->
            <!-- Optionally, you can add icons to the links -->
            <li class="{{activeMenu('home')}}">
                <a href="{{route('home')}}">
                    <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="{{activeMenu('sms.messages')}}">
                <a href="{{route('sms.messages')}}">
                    <i class="fa fa-envelope" aria-hidden="true"></i> <span>SMS</span>
                </a>
            </li>

            <li class="{{activeMenu('mobile.mobiles')}}">
                <a href="{{route('mobile.mobiles')}}">
                    <i class="fa fa-mobile-phone" aria-hidden="true"></i> <span>Mobile</span>
                </a>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
