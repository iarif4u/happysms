<?php
/**
 * Created by Md. Arifur Rahman
 * Date: 12/13/18
 * Time: 10:45 AM
 */
?>
@extends('admin.layouts.master')

@section('title',"General setting")

@section('header_left')
    Dashboard
    <small>Home Page</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Home Page</li>
@endsection

@section('header_title')
    Home
@endsection
@section('content')
    <div class="box box-body">
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    {{--<li><a href="#timeline" data-toggle="tab">Timeline</a></li>--}}
                    <li class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
                    <li><a href="#password" data-toggle="tab">Change Password</a></li>
                    <li><a href="#api_key" data-toggle="tab">API key</a></li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="profile">
                        {!! Form::open(['route' => 'profile', 'autocomplete'=>"off",'class'=>"form-horizontal"]) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('name', auth()->user()->name,['placeholder'=>"Name", 'class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('emal', 'Email',['class'=>'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::email('email', auth()->user()->email,['placeholder'=>"Email", 'class'=>'form-control','disabled']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('phone', 'Phone',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('phone', auth()->user()->phone,['placeholder'=>"Phone Number", 'class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('address', 'Address',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::textarea('address', auth()->user()->address,['placeholder'=>"Address", 'class'=>'form-control','rows'=>2,'cols'=>3]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="password">
                        <h4 class="text-center text-info">Change Password</h4>
                        <br>
                        {!! Form::open(['route' => 'password', 'autocomplete'=>"off",'class'=>"form-horizontal"]) !!}
                        <div class="form-group">
                            {!! Form::label('password', 'Password',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!!  Form::password('password', ['class'=>"form-control underlined",'placeholder'=>'Password']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Confirm',['class'=>'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!!  Form::password('password_confirmation', ['placeholder'=>'Retype password','class'=>"form-control underlined"]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="api_key">
                        <br>
                        {!! Form::open(['route' => 'api_generate', 'autocomplete'=>"off",'class'=>"form-horizontal api-form"]) !!}
                        <div class="form-group text-center">
                            <div class="row">
                                <div class="col-md-2">
                                    <h4 class="text-center text-info">API Key</h4>
                                </div>
                                <div class="col-md-7">
                                    <input readonly class="form-control" value="{{auth()->user()->api_key}}" type="text" name="api" id="api-token">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-2">
                                    {!! Form::submit('New Key',['class'=>'btn btn-primary btn-sm']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
@endsection
@section('script')
    <script>
        $(".api-form").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function (data) {
                    if (data.error == false) {
                        success_notify(data.message);
                        $("#api-token").val(data.api_key);
                    }else{
                        error_notify(data.message);
                    }
                }
            });
        })
        $("#api-token").click(function (e) {
            e.preventDefault();
            $("#api-token").prop("readonly", false);

            /* Get the text field */
            var copyText = document.getElementById("api-token");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");
            $("#api-token").prop("readonly", true);
            /* Alert the copied text */
            success_notify("Copied the text: " + copyText.value);
        });

    </script>
@endsection
