@extends('admin.layouts.master')

@section('title')
    Themehappy || SMS ADMIN PANEL
@endsection
@section('style')

    <link rel="stylesheet" type="text/css" href="{{asset('css/vendor/dataTables.bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('js/vendor/DataTables/Buttons-1.5.6/css/buttons.bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/datatables.mark.js/2.0.0/datatables.mark.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.min.css">
    <style>
        .dataTables_info, .dt-buttons {
            float: left;
        }
    </style>
@endsection
@section('header_left')
    Mobile
    <small>Mobile info</small>
@endsection

@section('header_right')
    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Mobile</li>
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="box-body">
                <div class="row">
                    <!-- Start Invite Staff List controls -->
                    <div class="box-body dc-table-style">
                        <div class="col-md-12 table table-responsive">
                            {!! $dataTable->table(['class' => 'table table-bordered table-condensed table-striped']) !!}
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- /End Invite Staff List controls -->
                </div>
                <!-- /.row -->
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript"
            src="{{asset('js/vendor/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('js/vendor/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('js/vendor/DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript"
            rc="{{asset('js/vendor/DataTables/Buttons-1.5.6/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('js/vendor/DataTables/Buttons-1.5.6/js/buttons.flash.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('js/vendor/DataTables/Buttons-1.5.6/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('js/vendor/DataTables/Buttons-1.5.6/js/buttons.print.min.js')}}"></script>
    <!-- Scripts -->
    <script src="{{asset('vendor/datatables/buttons.server-side.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/g/mark.js(jquery.mark.min.js),datatables.mark.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.13/features/mark.js/datatables.mark.js"></script>
    {!! $dataTable->scripts() !!}


    <script>
        $.extend(true, $.fn.dataTable.defaults, {
            mark: true
        });
        const mobile_socket = io(`${server_uri}/mobile`, {query: `user_id=${user_id}&email=${user_email}`});

        mobile_socket.on('connect', (data) => {
            mobile_socket.on('status', (data) => {
                reload_db_table();
            });
        });

    </script>
@endsection
