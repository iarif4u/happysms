<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> SMS Portal || Login Panel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--<link rel="apple-touch-icon" href="https://technext.github.io/modular-admin/apple-touch-icon.png">--}}
<!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('assets/extra/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Theme initialization -->
    <link rel="stylesheet" id="theme-style" href="{{asset('css/app.css')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/img/logo.jpg')}}"/>

    <link rel="stylesheet" id="theme-style" href="{{asset('assets/extra/app.css')}}">
    <style type="text/css">
        .auth-container .auth-header {
            text-align: center;

        }
        .btn.btn-primary {
            color: #fff;
            background-color: #00466f;
        }
        .btn.btn-primary:hover {
            color: #fff;
            background-color: #00466f;
            border-color: #00466f;

        }
        .btn.btn-primary {
            color: #fff;
            background-color: #00466f;
            border-color: #00466f;

        }
        .jqstooltip {
            position: absolute;
            left: 0px;top: 0px;
            visibility: hidden;
            background-color: rgba(0,0,0,0.6);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;font: 10px arial, san serif;text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }
        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }
    </style>
</head>
<body class="loaded">
<div class="auth">
    <div class="auth-container">
        <div class="card">
            <header class="auth-header">
                <h1 class="auth-title">
                    <div class="logo">
                        <img height="100" src="{{asset('assets/img/logo.jpg')}}" alt="">
                    </div>

                </h1>

            </header>
            <div class="auth-content padding-top-8 margin-top-10">
                <p class="text-bold text-info text-uppercase text-center text">Admin Login</p>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <span class="each-error">{{ $error }} </span><br/>
                        @endforeach
                    </div>

                @endif
                {!! Form::open(['route' => 'login','files' => false]) !!}
                <div class="form-group">
                    <label for="username">Email</label>

                    {!!  Form::text('email', null,['class'=>"form-control underlined",'placeholder'=>'Your email address','id'=>"dataName","aria-required"=>"true","required"=>""]) !!}
                </div>
                <div class="form-group">
                    <label for="password">Password</label>

                    {!!  Form::password('password', ['class'=>"form-control underlined",'placeholder'=>'Your password','id'=>"password","aria-required"=>"true","required"=>""]) !!}
                </div>
                <div class="form-group">
                    <label for="remember">
                        {!! Form::checkbox('remember', "","",['id'=>"remember",'class'=>'checkbox']) !!}
                        <span>Remember me</span>
                    </label>
                </div>
                <div class="form-group">

                    {!! Form::submit('Login',['class'=>"btn btn-block btn-primary"]); !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/extra/vendor.js')}}"></script>
<script src="{{asset('assets/extra/app.js')}}"></script>
<div class="responsive-bootstrap-toolkit">
    <div class="device-xs hidden-sm-up"></div>
    <div class="device-sm hidden-xs-down hidden-md-up"></div>
    <div class="device-md hidden-sm-down hidden-lg-up"></div>
    <div class="device-lg hidden-md-down hidden-xl-up"></div>
    <div class="device-xl hidden-lg-down"></div>
</div>
</body>
</html>
