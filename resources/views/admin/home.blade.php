@extends('admin.layouts.master')

@section('title')
    Themehappy || SMS ADMIN PANEL
@endsection
@section('style')
    <style>
        body {
            margin: 0px;
            padding: 0px;
            background: #f1ecec;
        }
    </style>
@endsection
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">

            <div class="row">

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{App\User::count()}}</h3>

                            <p>Admin</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="javascript:void(0);" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{\App\Model\Mobile::where(['connected'=>1])->count()}}</h3>
                            <p>Active Route</p>
                        </div>
                        <div class="icon padding-top-8">
                            <i class="fa fa-mobile-phone"></i>
                        </div>
                        <a href="{{url('route')}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>000</h3>

                            <p>OTP Vendor</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user-secret"></i>
                        </div>
                        <a href="{{url('otp_vendor')}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green-active">
                        <div class="inner">
                            <h3>{{\App\Model\Message::count()}}</h3>

                            <p>SMS Send</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <a href="{{url('report')}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <hr class="soket-info hide">

            <div class="row hide soket-info">
                <div class="col-md-5 cpu-info">
                    <h4 class="text align-items-lg-end">Operating System: <span class="os-type"></span></h4>
                    <h4>Time Online: <span class="time-online"></span></h4>

                    <h4 class="bold text-bold">Processor information</h4>
                    <div class="widget-text"><strong>Type:</strong> <span class="processor-type"></span></div>
                    <div class="widget-text"><strong>Number of Cores:</strong> <span class="processor-core"></span>
                    </div>
                    <div class="widget-text"><strong>Clock Speed:</strong><span class="processor-speed"></span></div>

                    <h4>Free Memory : <span class="free-meme"></span></h4>
                    <h4>Total Memory : <span class="total-meme"></span></h4>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6 cpu">

                            <div class="canvas-wrapper">
                                <canvas class="canvas" id="myCanvas" width="200" height="200"></canvas>
                                <div class="cpu-text"></div>
                            </div>

                            <h3>CPU Load</h3>
                        </div>
                        <div class="col-md-6 mem">

                            <div class="canvas-wrapper">
                                <canvas class="memCanvas" id="memCanvas" width="200" height="200"></canvas>
                                <div class="mem-text"></div>
                            </div>

                            <h3>Memory Usages</h3>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.row -->
        </div>
    </div>

@endsection
@section('script')
    <script>

        const cpu_socket = io(`${server_uri}/cpu`, {query: `user_id=${user_id}&email=${user_email}`});

        cpu_socket.on('connect', (data) => {
            cpu_socket.on('cpu_load', (data) => {
                $(".soket-info").removeClass('hide');
                set_cpu_inof(data.cpu_load);
                let freeMem = Math.floor(data.cpu_load.freeMem / 1073741824 * 100) / 100;
                let totalMem = Math.floor(data.cpu_load.totalMem / 1073741824 * 100) / 100;

                let memUse = Math.floor(data.cpu_load.memUse * 100);
                drawCircle(memUse, 'memCanvas', ".mem-text");
                drawCircle(data.cpu_load.cpuLoad, 'myCanvas', ".cpu-text");
                $(".free-meme").text(`${freeMem}GB`);
                $(".total-meme").text(`${totalMem}GB`);
            });
            setInterval(() => {
                cpu_socket.emit('get_cpu_load', {data: 'Get current cpu load'});
            }, 1000);

            socket.on('disconnect', () => {
                $(".soket-info").addClass('hide');
            });
        });

        socket.on('sms', (data) => {
            console.log('SMS', data);
            //this will console 'channel 2'
        });

        //establish connection
        socket.on('connect', (data) => {
            console.log('soket id: ', socket.id);
            socket.on('App\\Events\\UserLogin', (data) => {
                console.log("Output is:", data);
            })
        });
        //event fire when server is down or crash
        socket.on('connect_error', (error) => {
            console.log(server_uri, " has going down, for ", error);
        });
        //event fire when connection has time out
        socket.on('connect_timeout', (timeout) => {
            console.log(server_uri, " has timeout, for ", timeout);
        })
    </script>
@endsection
