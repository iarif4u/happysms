<?php

use App\Events\UserLogin;
use \Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/logout', 'Auth\LoginController@logout');
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'sms', 'as' => 'sms.'], function () {
        Route::get('/', 'Admin\SMS\SMSController@sms')->name('messages');
    });
    Route::group(['prefix' => 'mobile', 'as' => 'mobile.'], function () {
        Route::get('/', 'Admin\Mobile\MobileController@mobile')->name('mobiles');
    });
    Route::get('/profile', function () {
        return view('admin.profile');
    })->name('profile');
    Route::post('/profile', 'HomeController@update_profile')->name('profile');
    Route::post('/api/key/generate', 'HomeController@make_new_api_key')->name('api_generate');
    Route::get('/password', function () {
        return view('admin.profile');
    })->name('password');
});

Route::get('/name', function () {
    $user = auth()->user();
    event(new UserLogin($user));

});
//artisan queue:work redis
//npm install -g pm2
//pm2 start artisan --interpreter php --name queue-worker -- queue:work --daemon
//php /var/www/html/Message/artisan queue:work redis
