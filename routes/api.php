<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});


Route::post('message', 'API\MessageController@get_message');
Route::post('add-mobile', 'API\MobileController@add_new_mobile');
Route::post('disconnect-mobile', 'API\MobileController@disconnect_mobile');
Route::post('send-mobile', 'API\MobileController@send_sms_mobile');
Route::post('/sms/delivery', function () {
    return ['error' => false];
});
